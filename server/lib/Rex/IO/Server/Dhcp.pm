#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::IO::Server::Dhcp;
use Mojo::Base 'Mojolicious::Controller';

use Mojo::JSON "j";
use Mojo::UserAgent;
use Data::Dumper;

sub new_lease {
  my ($self) = @_;

  my $json = $self->req->json;

  $self->app->log->debug("adding new lease.");
  $self->app->log->debug( Dumper($json) );

  my $mac = $self->param("mac");
  unless ( $mac =~ m/^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/i ) {
    $self->app->log->error("Error adding new dhcp lease. no MAC given.");
    return $self->render(
      json   => { ok => Mojo::JSON->false, error => "No MAC given." },
      status => 500
    );
  }

  $mac =~ s/\-/:/g;

  my $res =
    $self->_ua->post_json( $self->config->{dhcp}->{server} . "/" . "\L$mac",
    $json )->res;

  $self->app->log->debug( "Got answer: " . Dumper($res) );

  $self->render( json => { ok => Mojo::JSON->true, data => $res } );
}

sub list_leases {
  my ($self) = @_;

  $self->app->log->debug("Listing dhcp leases.");
  my $res = $self->_list("/")->res->json;
  $self->app->log->debug( Dumper($res) );

  if ( !$res->{leases} ) {
    $self->app->log->debug("no dhcp leases found.");
    return $self->render( json => { ok => Mojo::JSON->true, data => {} } );
  }

  $self->render( json => { ok => Mojo::JSON->true, data => $res->{leases} } );
}

sub _ua {
  my ($self) = @_;
  return Mojo::UserAgent->new;
}

sub _list {
  my ( $self, $url ) = @_;
  my $tx =
    $self->_ua->build_tx( LIST => $self->config->{dhcp}->{server} . $url );
  $self->_ua->start($tx);
}

sub __register__ {
  my ( $self, $app ) = @_;
  my $r = $app->routes;

  $app->plugin("Rex::IO::Server::Mojolicious::Plugin::Dhcp");

  $r->post("/1.0/dhcp/mac/#mac")->over( authenticated => 1 )
    ->to("dhcp#new_lease");
  $r->get("/1.0/dhcp")->over( authenticated => 1 )->to("dhcp#list_leases");
}

1;
