#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::IO::Server::Helper::Dhcp;

use strict;
use warnings;

use Moo;
use Mojo::UserAgent;
use Try::Tiny;

has ua => (
  is      => 'ro',
  default => sub {
    return Mojo::UserAgent->new;
  }
);

has app => ( is => 'ro' );

sub get_mac_for {
  my ( $self, $ip ) = @_;
  my $tx = $self->ua->get( $self->app->config->{dhcp}->{server} . "/mac/$ip" );

  if ( my $res = $tx->success ) {
    my $mac = $res->json->{mac};
    $self->app->log->debug("Mac found: $mac");
    return $mac;
  }
  else {
    $self->app->log->debug("Mac not found.");
  }
}

sub delete_entry {
  my ( $self, $name ) = @_;
  try {
    $self->app->log->debug("Deleting dhcp entry: $name");
    $self->ua->delete( $self->app->config->{dhcp}->{server} . "/$name" );
  }
  catch {
    $self->app->log->debug("Error deleting dhcp entry: $name\n\nERROR: $_\n\n");
  };
}

1;
