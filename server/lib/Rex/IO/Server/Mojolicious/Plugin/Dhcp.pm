#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::IO::Server::Mojolicious::Plugin::Dhcp;

use strict;
use warnings;

use Mojolicious::Plugin;
use Rex::IO::Server::Helper::Dhcp;
use base 'Mojolicious::Plugin';

my $cache;

sub register {
  my ( $plugin, $app ) = @_;

  $app->helper(
    dhcp => sub {
      return Rex::IO::Server::Helper::Dhcp->new(app => $app);
    },
  );
}

1;
