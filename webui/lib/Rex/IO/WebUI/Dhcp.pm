package Rex::IO::WebUI::Dhcp;
use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;
use File::Basename;

# This action will render a template
sub show_leases {
  my ($self) = @_;
  $self->render;
}

##### Rex.IO WebUI Plugin specific methods
sub __register__ {
  my ( $self, $opt ) = @_;
  my $r      = $opt->{route};
  my $r_auth = $opt->{route_auth};
  my $app    = $opt->{app};

  $r_auth->get("/dhcp")->to("dhcp#show_leases");

  # add plugin template path
  push( @{ $app->renderer->paths }, dirname(__FILE__) . "/templates" );
  push( @{ $app->static->paths },   dirname(__FILE__) . "/public" );
}

1;
