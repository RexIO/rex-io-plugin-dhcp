# DHCP Plugin for Rex.IO

With this plugin you can manage DHCP servers within Rex.IO.

## API

Create a new lease:

```javascript
{
  "name": "foo.example.com",
  "ip"  : "192.168.1.7"
}
```

```
curl -D- -XPOST -d@lease.json \
  http://user:password@localhost:5000/1.0/dhcp/mac/$mac
```

List leases:

```
curl -D- -XGET \
  http://user:password@localhost:5000/1.0/dhcp
```
